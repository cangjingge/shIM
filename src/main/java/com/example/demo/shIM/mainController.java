package com.example.demo.shIM;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author jason
 */
@Controller
public class mainController {
    @RequestMapping("/")
    public String index(ModelMap map) {
        return "index";
    }
}
